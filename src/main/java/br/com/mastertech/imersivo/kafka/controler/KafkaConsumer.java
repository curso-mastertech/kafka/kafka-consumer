package br.com.mastertech.imersivo.kafka.controler;

import br.com.mastertech.imersivo.kafka.model.Acesso;
import br.com.mastertech.imersivo.kafka.service.AcessoLoggerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class KafkaConsumer {

    @Autowired
    private AcessoLoggerService acessoLoggerService;

//    @KafkaListener(topics = "padaria", groupId = "pedro")
//    public void recebe(@Payload Kafta kafta) {
//        System.out.println("Recebi uma kafta da: " + kafta.getMarca());
//    }

    @KafkaListener(topics = "pedro", groupId = "grupo1")
    public void recebeAcesso(@Payload Acesso acesso) {
        acessoLoggerService.registraAcesso(acesso);
    }

}
